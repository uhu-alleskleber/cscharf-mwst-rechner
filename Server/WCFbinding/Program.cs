﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace WCFbinding
{
    class Program
    {
        public const String Host = "net.tcp://localhost";
        public const Int32 Port = 31227;
        public const String ServiceName = "MwstService";

        static void Main(string[] args)
        {
            DBConnector dbConnector = DBConnector.Instance;

            Console.WriteLine(dbConnector);

            Console.WriteLine(dbConnector.getAllProductTypes()); // Gibt alle Produktypen und Ids zurück
            Console.WriteLine(dbConnector.getSteuerSatzByProdukt("3")); // Gibt Steuersatz für ProduktID zurück

            
            string svcAddress = Host + ":" + Port + "/" + ServiceName;
            Uri svcUri = new Uri(svcAddress);

            using (ServiceHost sh = new ServiceHost(typeof(CalcService), svcUri))
            {
                // Binding
                NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.Message);

                // Behavior
                ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
                sh.Description.Behaviors.Add(behavior);
                sh.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexTcpBinding(), "mex");

                //Endpoint
                sh.AddServiceEndpoint(typeof(ICalcService), tcpBinding, svcAddress);

                // Open
                sh.Open();

                Console.WriteLine("Service started '" + svcAddress + "' ...Press key to quit.");
                Console.ReadKey();

                Console.WriteLine("Quit. Press key to close.");
                Console.ReadKey();

                // Close
                sh.Close();
                
            }
            
        }
    }
}
