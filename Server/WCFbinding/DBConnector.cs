﻿using System.Data.SQLite;
using System;
using System.Collections.Generic;

namespace WCFbinding
{
    public class DBConnector
    {
        SQLiteConnection conn;

        private DBConnector()
        {
            conn = CreateConnection();
        }

        private SQLiteConnection CreateConnection()
        {
            conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True");
            try
            {
                conn.Open();
                Console.WriteLine("Connection established");
                InitializeDB();

                getAllProductTypes();

            } catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return conn;
        }

        private void InitializeDB()
        {
            SQLiteCommand query = conn.CreateCommand();

            // Drop Table
            Console.WriteLine("Droping Table steuersatz (if exists)...");
            query.CommandText = @"DROP TABLE IF EXISTS steuersatz";
            query.ExecuteNonQuery();

            // Create Table
            Console.WriteLine("Creating Table steuersatz...");
            query.CommandText = @"CREATE TABLE IF NOT EXISTS steuersatz(ID INTEGER NOT Null PRIMARY Key AUTOINCREMENT, 
                                                                        type_description VARCHAR(50) not null, 
                                                                        steuersatz FLOAT NOT null)";
            query.ExecuteNonQuery();

            // Insert Data
            Console.WriteLine("Inserting Data...");
            query.CommandText = @"INSERT INTO steuersatz(type_description, steuersatz)
                                  VALUES ('Steuerfrei', 0),
                                         ('reduziert ermaessigt', 0.05),
                                         ('ermaessigt', 0.07),
                                         ('Regelsteuersatz', 0.19),
                                         ('reduzierter Regelsteuersatz', 0.16),
                                         ('Land- und Forstwirtschaft', 0.055)";

            query.ExecuteNonQuery();
        }

        public Dictionary<String, String> getAllProductTypes()
        {
            /*
             Method returns a Dictonry of all Steuersätze in form Dictonary<ID, product_type>
             */

            Dictionary<String, String> output = new Dictionary<String, String>();

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();
            
            query.CommandText = "SELECT * FROM steuersatz";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                String id = datareader.GetInt32(0).ToString();
                String type_description = datareader.GetString(1);

                output[id] = type_description;
            }

            return output;
        }

        public double getSteuerSatzByProdukt(String produktTypeId)
        {
            /*
             Method returns Steuersatz for given produktTypeId
             */

            double output = 0;

            SQLiteDataReader datareader;
            SQLiteCommand query = conn.CreateCommand();

            query.CommandText = "SELECT steuersatz FROM steuersatz WHERE ID = " + produktTypeId + ";";

            datareader = query.ExecuteReader();
            while (datareader.Read())
            {
                output = datareader.GetDouble(0);
            }

            return output;
        }

        // Implementing Singleton pattern
        public static DBConnector Instance
        {
            get { return lazy.Value; }
        }

        private static readonly Lazy<DBConnector> lazy = new Lazy<DBConnector>(() => new DBConnector());

    }
}
