﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFbinding
{
    [ServiceContract]
    public interface ICalcService
    {
        [OperationContract]
        double calcBrutto(double NettoPreis, String produktID);
        [OperationContract]
        Dictionary<String, String> getAllProductTypes();
        [OperationContract]
        double getSteuerSatzByProdukt(String produktTypeId);
    }
}
