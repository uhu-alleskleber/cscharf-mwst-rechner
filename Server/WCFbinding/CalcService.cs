﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCFbinding
{
    public class CalcService : ICalcService
    {
        public double calcBrutto( double NettoPreis, String produktTyp)
        {
            double result = 0;
            produktTyp.Trim();
            double steuersatz = getSteuerSatzByProdukt(produktTyp);
            result = NettoPreis * (1 + steuersatz);
            return result;

        }

        public Dictionary<string, string> getAllProductTypes()
        {
            DBConnector dbConnector = DBConnector.Instance;
            return dbConnector.getAllProductTypes();
        }

        public double getSteuerSatzByProdukt(string produktTypeId)
        {
            DBConnector dbConnector = DBConnector.Instance;
            return dbConnector.getSteuerSatzByProdukt(produktTypeId);
        }
    }
}
