﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using WCF__Client_Mwst.ServiceReference1;

namespace WCF__Client_Mwst
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int heightElement;
        private bool changedbyUser;
        private Dictionary<String, String> produktArten;

        public event PropertyChangedEventHandler PropertyChanged;
        public const String Host = "net.tcp://localhost";
        public const Int32 Port = 31227;
        public const String ServiceName = "MwstService";
        ICalcService calcService;
        public MainWindow()
        {
            heightElement = 23;
            InitializeComponent();
            DataContext = this;
            //Loaded += Window_Loaded;
        }
        public int HeightElement
        {
            get
            {
                return heightElement;
            }
        }

        public Dictionary<string, string> ProduktArten { get => produktArten; set => produktArten = value; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                //creating the object of WCF service client         
                //ServiceReference1.CalcServiceClient mwstService = new ServiceReference1.CalcServiceClient();
                //Dictionary<String, String> test = mwstService.getAllProductTypes();
                // Address
                string svcAddress = Host + ":" + Port + "/" + ServiceName;

                // Binding
                NetTcpBinding tcpb = new NetTcpBinding(SecurityMode.Message);
                ChannelFactory<ICalcService> chFactory = new ChannelFactory<ICalcService>(tcpb);

                // Endpoint
                EndpointAddress epAddress = new EndpointAddress(svcAddress);

                // Create Channel
                calcService = chFactory.CreateChannel(epAddress);

                //Mappen ID des Steuersatzes + Wert des Steuersates
                produktArten = calcService.getAllProductTypes();
                for (int i = 1; i < produktArten.Count + 1; i++)
                {
                    string a = "";
                    produktArten.TryGetValue(i.ToString(), out a);
                    Produkart.Items.Add(produktArten[i.ToString()]);
                }
                Console.WriteLine("Service Abgeschlossen");
            }
            catch
            {

            }

        }


        private void InputBrutto_GotFocus(object sender, RoutedEventArgs e)
        {
            InputBrutto.Text = InputBrutto.Text.Trim();
            //waterMarkedText.Visibility = Visibility.Collapsed;
            InputBrutto.Visibility = Visibility.Visible;
            if (string.IsNullOrEmpty(InputBrutto.Text))
            {
                InputBrutto.Foreground = Brushes.Gray;
            }
            else
            {
                // wenn Changed by user Falsch --> dann leerzeichen ansonsten bleibt es so wie es war
                
                if (changedbyUser == false)
                {
                    InputBrutto.Text = "";
                }
                changedbyUser = true;
                InputBrutto.Foreground = Brushes.Black;
            }
            InputBrutto.Focus();
        }

        private void InputBrutto_LostFocus(object sender, RoutedEventArgs e)
        {
            InputBrutto.Text= InputBrutto.Text.Trim();
            if (string.IsNullOrEmpty(InputBrutto.Text))
            {
                //InputBrutto.Visibility = Visibility.Collapsed;
                //waterMarkedText.Visibility = Visibility.Visible;
                InputBrutto.Text = "Eingabe des Nettowertes";
                InputBrutto.Foreground = Brushes.Gray;
                changedbyUser = false;

            }
            else
            {
                InputBrutto.Foreground = Brushes.Black;
            }
        }

        private void Calc_Click(object sender, RoutedEventArgs e)
        {
            double input = 0;
            double erg = 0;
            String selected = "";
            int selectedIndex;
            try {
                input = Convert.ToDouble(InputBrutto.Text);
            }
            catch
            {
                MessageBox.Show("Bitte Geben Sie eine Zahl ein");
            }
            if (Produkart.SelectedIndex > -1)
            {
                selected = Produkart.SelectedValue.ToString();
                //Für Methode brauche ich ID über das Dict
                selectedIndex = Produkart.SelectedIndex+1;
                selected = selectedIndex.ToString();
                erg = calcService.calcBrutto(input, selected);
                ErgTxt.Content = erg.ToString() + " €";
            }
            else
            {
                MessageBox.Show("Bitte Wählen Sie etwas aus der Liste aus");
            }
            //Console.WriteLine(produktArten["1"]);
        }

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
